package th.ac.kku.charoenkitsupat.chanyanood.webapicaller;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    private EditText urlVal;
    private String urlValStr;
    //private String API_URL = "https://maps.googleapis.com/maps/api/geocode/json?address=Khon%20Kaen&key=AIzaSyD2Tr0hKSdsQF8ljVw9iS5E7AUbDtpO7PY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        urlVal = (EditText)findViewById(R.id.urlText);
    }

    public void clickSearch(View v) {
        urlValStr = urlVal.getText().toString();
        String API_URL = "";
        if(!urlValStr.equalsIgnoreCase("http://ip.jsontest.com/") && !urlValStr.equalsIgnoreCase("http://www.kku.ac.th")) {
            API_URL = API_URL.concat("https://maps.googleapis.com/maps/api/geocode/json?address=");
            API_URL = API_URL.concat(urlValStr.replace(" ", "%20"));
            API_URL = API_URL.concat("&key=AIzaSyD2Tr0hKSdsQF8ljVw9iS5E7AUbDtpO7PY");
            urlValStr = API_URL;
        }
        new RetrieveFeedTask().execute(urlValStr);
    }

    // AsyncTask String, the same at doInBackground
    class RetrieveFeedTask extends AsyncTask<String, Void, String> {
        //private Exception ex;
        private ProgressBar progressBar;
        private TextView responseView;


        protected void onPreExecute() {
            progressBar = (ProgressBar)findViewById(R.id.progressBarId);
            responseView = (TextView)findViewById(R.id.responseViewId);

            progressBar.setVisibility(View.VISIBLE);
            responseView.setText("");
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                Log.i("INFO", "url = " + params[0]);
                URL urlAddr = new URL(params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) urlAddr.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    return  stringBuilder.toString();
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                Log.e("Error", e.getMessage(), e);
                return null;
            }
        }

        @Override
        protected  void onPostExecute(String response) {
            if(response == null) {
                response = "There was an error";
            }
            progressBar.setVisibility(View.GONE);
            Log.i("INFO", "response = " + response);
            if (urlValStr.equalsIgnoreCase("http://www.kku.ac.th")) {
                responseView.setText(response); // problem 3
            } else {
                try {
                    JSONObject obj = (JSONObject) new JSONTokener(response).nextValue();
                    if (urlValStr.equalsIgnoreCase("http://ip.jsontest.com/")) {
                        String ipAddr = (String) obj.get("ip");
                        responseView.setText(ipAddr); // problem 4
                    } else {// problem 5
                        /*
                        String jsonData = obj.toString().substring(1, obj.toString().length() - 1);
                        responseView.setText(jsonData); */
                        JSONArray results  = obj.getJSONArray("results");
                        JSONObject locinfo = results.getJSONObject(0);
                        JSONObject geometry = locinfo.getJSONObject("geometry");
                        JSONObject location = geometry.getJSONObject("location");
                        String lat = location.getString("lat");
                        String lng = location.getString("lng");
                        responseView.setText("Latitude of " + urlVal.getText().toString() + " = " + lat + "\n"
                                + "Longitude of " + urlVal.getText().toString() + " = " + lng);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
