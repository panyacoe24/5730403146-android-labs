package th.ac.kku.charoenkitsupat.chanyanood.readfiledemo;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class MainActivity extends ListActivity {
    TextView SelectedApp;
    ArrayList<String> appItems = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SelectedApp = (TextView)findViewById(R.id.selectionId);

        try {
            InputStream readSrcFile = getResources().openRawResource(R.raw.apps);
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(readSrcFile, null);
            NodeList apps = doc.getElementsByTagName("app");
            for(int i = 0; i < apps.getLength(); i++) {
                appItems.add(((Element) apps.item(i)).getAttribute("name"));
            }
        } catch (Exception ex) {
            Toast.makeText(this, "Exception" + ex.toString(), Toast.LENGTH_LONG).show();
        }
        setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, appItems));
    }

    public void onListItemClick(ListView parent, View view, int position, long id) {
        SelectedApp.setText(appItems.get(position).toString());
    }
}
