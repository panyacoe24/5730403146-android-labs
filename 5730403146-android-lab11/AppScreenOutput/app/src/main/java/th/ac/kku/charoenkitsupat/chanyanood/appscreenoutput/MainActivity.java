package th.ac.kku.charoenkitsupat.chanyanood.appscreenoutput;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends Activity {
    private EditText editText;
    private Button rdBtn, wrBtn;
    static final int READ_BLOCK_SIZE = 100;
    public  static final String TAG = MainActivity.class.getSimpleName();
    String filename = "myCakeMenu", fileInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initInstances();
    }

    protected void initInstances() {
        editText = (EditText)findViewById(R.id.edit_file_id);
        editText.setSelection(0);
        fileInfo = getFileStreamPath(filename).getAbsolutePath();

        wrBtn = (Button)findViewById(R.id.btn_wr_id);
        rdBtn = (Button)findViewById(R.id.btn_rd_id);
    }

    public void writeListener (View v) {
        writeFile(v);
    }

    public void readListener (View v) {
        readFile(v);
    }

    // Write data to a file
    public void writeFile(View v) {
        try {
            FileOutputStream fileOut = openFileOutput(filename, MODE_PRIVATE);
            OutputStreamWriter outputWriter = new OutputStreamWriter(fileOut);
            String data = editText.getText().toString();
            outputWriter.write(data);
            outputWriter.close();
            fileOut.close();
            String msg = "write " + data + "to file " + fileInfo;
            Log.d(TAG, msg);
            Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG).show();
        } catch (Exception exWr) {
            exWr.printStackTrace();
        }
    }

    // Read data from a file
    public  void readFile(View v) {
        String line;
        StringBuffer output = new StringBuffer();
        try{
            FileInputStream fileIn = openFileInput(filename);
            InputStreamReader inputReader = new InputStreamReader(fileIn);
            BufferedReader bufferedReader = new BufferedReader(inputReader);
            while ((line = bufferedReader.readLine()) != null) {
                output.append(line);
                Log.d(TAG, "Read " + line);
            }
            inputReader.close();
            bufferedReader.close();
            fileIn.close();
            editText.setText(output);
            String msg = "read " + output + " from file " + fileInfo;
            Log.d(TAG, msg);
            Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG).show();
        } catch (Exception exRd) {
            exRd.printStackTrace();
        }
    }
}
