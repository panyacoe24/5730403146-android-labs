package th.ac.kku.charoenkitsupat.chanyanood.simplegeocoder;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener, View.OnClickListener {
    //A request code for Google Play services
    private static final String TAG = "LocationTest";
    private String addressStr, latitudeStr, longitudeStr;

    String result = null;
    double latitudeDoub, longitudeDoub;
    EditText editLatitude, editLongitude, editAddress;
    RadioGroup radioOptionGroup;
    RadioButton radioLatLng, radioAddress;
    Button fetchButton;
    TextView infoText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //EditText latitude, longitude and address
        editLatitude = (EditText) findViewById(R.id.latitudeEditID);
        editLongitude = (EditText) findViewById(R.id.longitudeEditID);
        editAddress = (EditText) findViewById(R.id.addressEditID);

        infoText = (TextView) findViewById(R.id.resultTextID);

        //focus and enable edit text
        editLatitude.requestFocus();
        editLongitude.requestFocus();

        editLatitude.setEnabled(true);
        editLongitude.setEnabled(true);
        editAddress.setEnabled(false);

        //Button and radio listener
        radioOptionGroup = (RadioGroup) findViewById(R.id.optionRdID);
        radioLatLng = (RadioButton) findViewById(R.id.latAndLngRdID);
        radioAddress = (RadioButton) findViewById(R.id.addressRdID);
        radioOptionGroup.setOnCheckedChangeListener(this);

        fetchButton = (Button) findViewById(R.id.fetchBtnID);
        fetchButton.setOnClickListener(this);
    }

    public void getLocationFromAddress(String addressInput) {
        Geocoder geocoder = new Geocoder(this);
        List<Address> address;

        try {
            address = geocoder.getFromLocationName(addressInput, 5);

            Address location = address.get(0);

            latitudeDoub = location.getLatitude();
            longitudeDoub = location.getLongitude();

            infoText.setText("Lat:" + latitudeDoub + " Lng:" + longitudeDoub);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void getAddressFromLocation(final Location location, final Context context, final Handler handler){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                try {
                    //getting address from Location
                    List<Address> list = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                    if(list != null && list.size() > 0) {
                        Address address = list.get(0);
                        result = address.getAddressLine(0) + ", " + address.getLocality();
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Impossible to connect to Geocoder", e);
                } finally {
                    Message msg = Message.obtain();
                    msg.setTarget(handler);
                    if(result != null) {
                        msg.what = 2;
                        Bundle bundle = new Bundle();
                        bundle.putString("addressFromLoc", result);
                        msg.setData(bundle);
                    } else
                        msg.what = 0;
                    msg.sendToTarget();
                }
            }
        });
        thread.start();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if(checkedId == R.id.latAndLngRdID) {
            radioLatLng.isChecked();
            editLatitude.requestFocus();
            editLongitude.requestFocus();

            editLatitude.setEnabled(true);
            editLongitude.setEnabled(true);
            editAddress.setEnabled(false);
        } else if (checkedId == R.id.addressRdID) {
            radioAddress.isChecked();
            editAddress.requestFocus();

            editAddress.setEnabled(true);
            editLatitude.setEnabled(false);
            editLongitude.setEnabled(false);
        }
    }

    @Override // Button handling
    public void onClick(View v) {
        if (v == fetchButton) {
            if (radioLatLng.isChecked()) {

                latitudeStr = editLatitude.getText().toString();
                longitudeStr = editLongitude.getText().toString();

                try {
                    latitudeDoub = Double.parseDouble(latitudeStr);
                    longitudeDoub = Double.parseDouble(longitudeStr);
                } catch (NumberFormatException e) {
                    // Nothing to do
                }
                //The call to this Geocoder procedure in UI Activity
                //infoText.setText("SimpleGeocoder lat : " + longitudeDoub + "lng: " + latitudeDoub);

                final Location loc = new Location("Khon Kaen University");
                loc.setLatitude(latitudeDoub);
                loc.setLongitude(longitudeDoub);
                getAddressFromLocation(loc, this, new GeocoderHandler());

                //replace by what you need to do
                infoText.setText(result);
            } else if (radioAddress.isChecked()) {
                addressStr = editAddress.getText().toString();
                getLocationFromAddress(addressStr);
            }
        }
    }

    private final class GeocoderHandler extends Handler {
        @Override
        public  void handleMessage(Message message) {
            Bundle bundle;
            switch (message.what) {
                case 1:
                    bundle = message.getData();
                    result = bundle.getString("addressFromName");
                    break;
                case 2:
                    bundle = message.getData();
                    result = bundle.getString("addressFromLoc");
                    break;
                default:
                    result = null;
            }
        }
    }
}
