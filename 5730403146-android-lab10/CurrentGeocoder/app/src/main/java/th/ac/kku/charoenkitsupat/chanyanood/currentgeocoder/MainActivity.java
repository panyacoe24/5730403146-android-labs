package th.ac.kku.charoenkitsupat.chanyanood.currentgeocoder;

import android.content.Context;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, LocationListener{
    //A request code for Google Play services
    private static final String TAG = MainActivity.class.getSimpleName();
    private LocationManager locationManager;
    private Location loc;
    private String addressStr, latitudeStr, longitudeStr;

    String provider;
    double latitudeDoub, longitudeDoub;
    EditText editLatitude, editLongitude, editAddress;
    Button fetchButton;
    TextView infoText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //EditText latitude, longitude and address
        editLatitude = (EditText) findViewById(R.id.latitudeEditID);
        editLongitude = (EditText) findViewById(R.id.longitudeEditID);
        editAddress = (EditText) findViewById(R.id.addressEditID);

        fetchButton = (Button) findViewById(R.id.fetchBtnID);
        fetchButton.setOnClickListener(this);
    }

    public void getCurrentLocation() {
        // Get the location manager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // Define the criteria how to select the locatioin provider -> use
        // default
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        try {
            loc = getLastKnownLocation();
        } catch (SecurityException se) {

        }

        if (loc != null) {
            System.out.println("Provider " + provider + " has been selected.");
            onLocationChanged(loc);
        } else {
            editLatitude.setText("Location not available");
            editLongitude.setText("Location not available");
        }
    }

    public Location getLastKnownLocation() {
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        Location loc = null;
        for (String provider : providers) {
            try {
                loc = locationManager.getLastKnownLocation(provider);
                //Log.d("last known location, provider: %s, location: %s", provider, l);
            } catch (SecurityException se) {

            }
            if (loc == null) {
                continue;
            }
            if (bestLocation == null
                    || loc.getAccuracy() < bestLocation.getAccuracy()) {
                //ALog.d("found best last known location: %s", l);
                bestLocation = loc;
            }
        }
        if (bestLocation == null) {
            return null;
        }
        return bestLocation;
    }

    public void getLocationFromAddress() {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses;

        try {
            addresses = geocoder.getFromLocation(latitudeDoub, longitudeDoub, 1);

            // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String address = addresses.get(0).getAddressLine(0);
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

            editAddress.setText(address + " " + city + " " + state + " " + country + " " + postalCode + " " + knownName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override // Button handling
    public void onClick(View v) {
        if (v == fetchButton) {
            getCurrentLocation();
            getLocationFromAddress();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        latitudeDoub = location.getLatitude();
        longitudeDoub = location.getLongitude();

        editLatitude.setText("Latitude: " + String.valueOf(latitudeDoub ));
        editLongitude.setText("Longitude: " + String.valueOf(longitudeDoub));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
