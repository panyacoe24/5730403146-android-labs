package th.ac.kku.charoenkitsupat.chanyanood.currentlocationdisplay;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity implements LocationListener {

    protected LocationManager locationManager;
    private  Location loc;

    TextView latText, lngText;
    double lat, lng;
    String provider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        latText = (TextView)findViewById(R.id.latTextId);
        lngText = (TextView)findViewById(R.id.lngTextId);

        // Get the location manager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // Define the criteria how to select the locatioin provider -> use
        // default
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        try {
            loc = getLastKnownLocation();
        } catch (SecurityException se) {

        }

        if (loc != null) {
            System.out.println("Provider " + provider + " has been selected.");
            onLocationChanged(loc);
        } else {
            latText.setText("Location not available");
            lngText .setText("Location not available");
        }
    }

    public Location getLastKnownLocation() {
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        Location loc = null;
        for (String provider : providers) {
            try {
                loc = locationManager.getLastKnownLocation(provider);
                //Log.d("last known location, provider: %s, location: %s", provider, l);
            } catch (SecurityException se) {

            }
            if (loc == null) {
                continue;
            }
            if (bestLocation == null
                    || loc.getAccuracy() < bestLocation.getAccuracy()) {
                //ALog.d("found best last known location: %s", l);
                bestLocation = loc;
            }
        }
        if (bestLocation == null) {
            return null;
        }
        return bestLocation;
    }

    @Override
    public void onLocationChanged(Location location) {
        lat = location.getLatitude();
        lng = location.getLongitude();

        latText.setText("Latitude: " + String.valueOf(lat));
        lngText.setText("Longitude: " + String.valueOf(lng));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d("Latitude", "status");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("Latitude", "disable");
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("Latitude", "disable");
    }
}
