package th.ac.kku.charoenkitsupat.chanyanood.implicitintents;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Spinner spinner;
    Button trig_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        spinner = (Spinner)findViewById(R.id.spinner);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.intents, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        trig_btn = (Button)findViewById(R.id.trigger);
        trig_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int pos = spinner.getSelectedItemPosition(); // for new string resource
        Intent intent = null;
        switch (pos) {
            case 0:
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.kku.ac.th"));
                break;
            case 1:
                intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:(+43)009700"));
                break;
            case 2:
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0.0?q=Khon Kaen University"));
                break;
            case 3:
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("content://contacts/people"));
                break;
        }
        if(intent != null) {
            startActivity(intent);
        }
    }
}
