package th.ac.kku.charoenkitsupat.chanyanood.activitycommunication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {
    TextView msg;
    EditText rely;
    String msg_str, rely_str;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        setTitle("Result Activity");

        msg = (TextView)findViewById(R.id.receive_id);
        rely = (EditText)findViewById(R.id.send_id);

        Intent intent = getIntent();
        msg_str = intent.getStringExtra("send_name_to_result");
        msg.setText(msg_str);
    }

    @Override
    public  void finish() {
        //create new Intent
        Intent rstToMain = new Intent(ResultActivity.this, MainActivity.class);
        //read the data of the EditText with the id returnValue
        rely_str = rely.getText().toString();
        //put the text from EditText as String extra into the intent
        rstToMain.putExtra("send_result_to_main", rely_str);
        //use setResult(RESULT_OK,intent); to return the Intent to the application
        setResult(RESULT_OK, rstToMain);
        super.finish();
    }
}
