package th.ac.kku.charoenkitsupat.chanyanood.activitycommunication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText name;
    TextView msg;
    String name_str;
    //constant to determine which sub-activity returns
    private static final int REQUEST_CODE = 10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void okButtonClicked(View view) {
        name = (EditText)findViewById(R.id.edit_id);
        msg = (TextView)findViewById(R.id.msg_text_id);
        name_str = name.getText().toString();
        //create new intent
        Intent mainToRst = new Intent(MainActivity.this,ResultActivity.class);
        mainToRst.putExtra("send_name_to_result", name_str);
        //use startActivityForResult
        startActivityForResult(mainToRst, REQUEST_CODE);
        //with REQUEST_CODE
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == REQUEST_CODE) {
            //get data from ResultActivity
            if(data.hasExtra("send_result_to_main")) {
                //set the value of TextView with the received data
                msg.setText(data.getExtras().getString("send_result_to_main"));
            }
        }
    }
}
