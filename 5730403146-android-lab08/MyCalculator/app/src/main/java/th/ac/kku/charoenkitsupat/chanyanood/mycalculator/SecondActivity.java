package th.ac.kku.charoenkitsupat.chanyanood.mycalculator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    TextView result_text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent intent = getIntent();
        double result_db = intent.getExtras().getDouble("result");
        String result_str = String.valueOf(result_db);
        result_text = (TextView) findViewById(R.id.result_id);
        result_text.setText("Result = " + result_str);
    }
}
