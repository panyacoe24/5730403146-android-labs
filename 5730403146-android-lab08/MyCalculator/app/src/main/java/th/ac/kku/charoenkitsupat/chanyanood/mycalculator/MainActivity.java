package th.ac.kku.charoenkitsupat.chanyanood.mycalculator;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener {
    EditText first_edit, second_edit;
    TextView result_text;
    Button calculate_btn;
    String first_str, second_str;
    RadioGroup op_grp;
    Switch switch_ctrl;
    double first_db, second_db, result_db;
    boolean isUpdated = false, isNumeric, isdivide;
    float start_time, end_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        displayScreenSize();

        first_edit = (EditText) findViewById(R.id.st_num_id);
        second_edit = (EditText) findViewById(R.id.nd_num_id);
        result_text = (TextView) findViewById(R.id.result_id);

        calculate_btn = (Button) findViewById(R.id.cal_id);
        op_grp = (RadioGroup) findViewById(R.id.operators_id);
        switch_ctrl = (Switch) findViewById(R.id.switch_id);

        calculate_btn.setOnClickListener(this);
        op_grp.setOnCheckedChangeListener(rgListener);
        switch_ctrl.setOnCheckedChangeListener(swListener);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            showToast("Choose action settings");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void acceptNumbers() {
        // Handles and checks whether entered text are number.
        isNumeric = first_str.matches("[+-]?([0-9]*[.])?[0-9]+") &&
                second_str.matches("[+-]?([0-9]*[.])?[0-9]+");
        isdivide = second_db != 0;

        if (!isNumeric) {
            showToast("Please enter only a number");
        }
    }

    private void showToast(String msg) {
        // Shows toasts that are displayed in this app.
        if (!isNumeric || !isdivide) {
            Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG).show();
        }
    }

    private void calculate(int id) {
        /* Accepts the id of the chosen operator, performs the arithmetic
        operation, and updates the result.
         */
        start_time = System.currentTimeMillis(); // Start to measure time

        first_str = first_edit.getText().toString();
        second_str = second_edit.getText().toString();

        try {
            first_db = Double.parseDouble(first_str);
            second_db = Double.parseDouble(second_str);
        } catch (NumberFormatException e) {
            // Nothing
        }

        acceptNumbers();

        if (isNumeric) {
            // check operators
            switch (id) {
                case R.id.plus_id:
                    result_db = first_db + second_db;
                    break;
                case R.id.minus_id:
                    result_db = first_db - second_db;
                    break;
                case R.id.times_id:
                    result_db = first_db * second_db;
                    break;
                case R.id.devided_id:
                    if (isdivide) {
                        result_db = first_db / second_db;
                    } else {
                        showToast("Please divide by non-zero number");
                    }
                    break;
            }
            result_text.setText("=" + result_db);

            end_time = System.currentTimeMillis(); // Stop to measure time.
            Log.d("Calculator", "Computation time = " + (end_time - start_time));
        }
    }

    public void onClick(View view) {
        // Handling method for the button
        if (view == calculate_btn) {
            if (isUpdated) {
                calculate(op_grp.getCheckedRadioButtonId());
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                intent.putExtra("result", result_db);
                startActivity(intent);
            }
        }
    }

    RadioGroup.OnCheckedChangeListener rgListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int id) {
            // Handling method for the radio group
            if (isUpdated) {
                calculate(id);
            }
        }
    };

    Switch.OnCheckedChangeListener swListener = new Switch.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                isUpdated = true;
                switch_ctrl.setText("ON");
                calculate(op_grp.getCheckedRadioButtonId());
            } else {
                isUpdated = false;
                switch_ctrl.setText("OFF");
            }
        }
    };

    public void displayScreenSize() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x; // Screen Width
        int height = size.y; // Screen Height

        showToast("Width = " + width + ", Height = " + height);
    }

    @Override
    protected  void onSaveInstanceState(Bundle outState) {
        // Save thing(s) here
        outState.putString("op1", first_edit.getText().toString());
        outState.putString("op2", second_edit.getText().toString());
        outState.putString("result", result_text.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        first_edit.setText(savedInstanceState.getString("op1"));
        second_edit.setText(savedInstanceState.getString("op2"));
        result_text.setText(savedInstanceState.getString("result"));
    }
}
