package th.ac.kku.charoenkitsupat.chanyanood.applylayout;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Iterator;

import static java.lang.System.exit;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    HashMap<String, String> account = new HashMap<String, String>();
    Button login_btn;
    EditText username_edit, password_edit;
    String username_str, password_str;
    Boolean isRightPassword = false, exitAccount = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        login_btn = (Button) findViewById(R.id.log_btn_id);
        login_btn.setOnClickListener(this);

        //database
        account.put("PanyaTest", "p3a1n4y6a");
        account.put("BebTest", "3B0e2b9");
    }

    public void checkUserAndPassword() {
        username_edit = (EditText) findViewById(R.id.user_edit_id);
        password_edit = (EditText) findViewById(R.id.passwd_edit_id);

        username_str = username_edit.getText().toString().trim();
        password_str = password_edit.getText().toString().trim();

        // Finding in database
        Iterator<String> checkAcc = account.keySet().iterator();
        while(checkAcc.hasNext()){
            String key = checkAcc.next();
            if (key.equals(username_str)) {
                String value = account.get(key);
                if (value.equals(password_str)) {
                    isRightPassword = true;
                } else {
                    exitAccount = true;
                }
                break;
            }
        }
    }

    public void showMessageOnToast(String msg) {
        Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.log_btn_id) {

            checkUserAndPassword();

            if (isRightPassword) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent mapActivity = new Intent(LoginActivity.this, MapActivity.class);
                        startActivity(mapActivity);
                        finish();
                    }
                }, 100);
            } else {
                if (exitAccount) {
                    showMessageOnToast("Your password is not correct, try again");
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent regisActivity = new Intent(LoginActivity.this, RegisterActivity.class);
                            startActivity(regisActivity);
                            finish();
                        }
                    }, 100);
                }
            }
        }
    }
}
